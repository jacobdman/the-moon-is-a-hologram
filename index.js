const { app, start } = require('boilerpress');
const os = require('os');
// Add any middleware you need
app.use((req, res, next) => {
  res.locals = {
    host: os.hostname(),
    title: 'Express',
  };
  next();
});
app.use((req, res) => {
  res.send('Wake up sheeple');
});
// Start the Express server
start();